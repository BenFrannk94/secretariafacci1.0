const email = document.getElementById("email");
const contrasenia = document.getElementById("password");
const m = document.getElementById("mantenerSesion");
const formulario = document.getElementById("form");
const listInsputs = document.querySelectorAll(".form-input");

form.addEventListener("submit", (e) => {
    e.preventDefault();
    let condicion = validaciondatos();
    if(condicion){
        enviarFormulario();
    }
});

function validaciondatos() {
    formulario.lastElementChild.innerHTML = "";
    let condicion = true;
    listInsputs.forEach(element => {
        element.lastElementChild.innerHTML= "";
    });

    if(email.value.lenght < 1 || email.value.trim() == "") {
        muestraError("email", "Correo electrónico no válido*");
        condicion = false; 
    }
    if(contrasenia.value.lenght < 1 || contrasenia.value.trim() == "") {
        muestraError("password", "Contraseña no válida*");
        condicion = false; 
    }
    return condicion;
}
function muestraError(claseInput, mensaje) {
    let elemento = document.querySelector(`.${claseInput}`);
    elemento.lastElementChild.innerHTML = mensaje;
}
function enviarFormulario(){
    formulario.reset();
    formulario.lastElementChild.innerHTML = "¡Inicio de Sesión Exitoso!";
}